package fr.malta.websocket.server;

import org.java_websocket.server.DefaultSSLWebSocketServerFactory;
import org.java_websocket.server.WebSocketServer;
import org.java_websocket.util.Base64;

import javax.imageio.ImageIO;
import javax.net.ssl.SSLContext;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main {
    public static void main(String[] args) {
        if (!System.getProperty("os.name").equals("Mac OS X")) {
            //On met le chemin par défaut dans le répertoire en cours
            SimpleServer.basePath = "";
            //Sous windows, cette ligne permet de mettre une entrée de log sur une seule ligne
            System.setProperty("java.util.logging.SimpleFormatter.format",
                    "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
        } else {
            //Sous mac, on change le chemin par défaut pour le mettre dans /tmp/
            SimpleServer.basePath = "/tmp/";
            //On lance la mise à jour ici pour mac, vu qu'on ne peut pas mettre plusieurs .jar dans un package
            //La solution la plus simple a été de mettre le .jar de mise à jour en dépendance de celui-ci
            fr.malta.update.Main.main(args);
        }

        Logger logger = Logger.getLogger("LogWebsocket");
        try {
            FileHandler fh = new FileHandler(SimpleServer.basePath + "general.log",true);
            logger.addHandler(fh);
            logger.setUseParentHandlers(false);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //On défini l'URL et les différents ports pour le serveur
        String host = "localhost";
        int port = 5001;
        int securePort = 5002;
        try {
            //On récupère le contexte SSL, qui permettra de gérer le certificat lors de la connection entre le client et le serveur
            SSLContext sslContext = SimpleServer.getSslContext();

            //On initialise l'icône de la barre de notification
            TrayIcon trayIcon = intoSysTray();

            //On initialise et on lance le serveur de websocket non sécurisée
            WebSocketServer server;
            server = new SimpleServer(new InetSocketAddress(host, port), trayIcon);
            server.start();

            //On initialise et on lance le serveur de websocket sécurisée
            WebSocketServer secureServer;
            secureServer = new SimpleServer(new InetSocketAddress(host, securePort), trayIcon);
            secureServer.setWebSocketFactory(new DefaultSSLWebSocketServerFactory(sslContext));
            secureServer.start();
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException | UnrecoverableKeyException | KeyManagementException e) {
            logger.severe(e.getMessage());
        }
    }

    protected static TrayIcon intoSysTray() throws IOException {
        //Check if SystemTray is supported
        if (!SystemTray.isSupported()) {
            System.out.println("SystemTray is not supported");
        } else {
            final PopupMenu popup = new PopupMenu();
            final TrayIcon trayIcon = new TrayIcon(getSysTrayImage());
            final SystemTray tray = SystemTray.getSystemTray();

            //Create pop-up menu components
            MenuItem exitItem = new MenuItem("Exit");

            //Add components to pop-up menu
            popup.add(exitItem);

            trayIcon.setPopupMenu(popup);

            exitItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    tray.remove(trayIcon);
                    System.exit(0);
                }
            });

            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                e.printStackTrace();
            }

            return trayIcon;
        }
        return null;
    }

    protected static Image getSysTrayImage() throws IOException {
        //On récupère l'icône de la barre de notification en base64 (cela permet de ne pas la mettre en ressource)
        String b64 = "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsSAAALEgHS3X78AAABFklEQVQ4y6WTMVLDMBBF367sIm5dOA0p7TuEY9CFm0BK4CTcIDTUTHKIpAtt3IbCkkWBkRXGNkX+zBZarf78/3ckh8PB8wciEgrAe0/btgwhiQ91XbPb7vrHHQHe47ta3i7J83yY4HSq2WzemEJZVeMEWTajqioQ+DqfOR4/AVgsbphlGfifmQu7cQYigjEGYwz7/Z7npxcAHtcPlGWJcw7nHN73selQeKqKiEZ97Xp9sKMEqtpVP6ja90cJ4tX9Do8piEl0SL6qopEFE1Tp/xZCRRZE5fJuSkFQESnQCQvJmIJiXrC6XyFAMS8GHwPI6/uHB8L+0yQlSRPSJA3SnXNYa7GNpbEN1trwN4S7tecKKFfiaoJvrjxQ/YpN1eIAAAAASUVORK5CYII=";
        byte[] bytes = Base64.decode(b64);
        return ImageIO.read(new ByteArrayInputStream(bytes));
    }
}
