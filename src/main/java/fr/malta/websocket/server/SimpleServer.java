package fr.malta.websocket.server;

import fr.malta.crypto.GenSig;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.awt.*;
import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

//Le fonctionnement est assez similaire à celui du client (dans TitanWebMedecin), on définit le serveur puis les évènements associés
public class SimpleServer extends WebSocketServer {
    public static String basePath;
    private Logger logger = Logger.getLogger("LogWebsocket");
    private boolean prepareToSign = false;
    private String checksum;
    private TrayIcon trayIcon;

    public SimpleServer(InetSocketAddress address, TrayIcon trayIcon) throws IOException {
        super(address);
        //On prépare le fichier de log
        if (!Files.exists(Paths.get(basePath + "websocket.log"))) {
            Files.createFile(Paths.get(basePath + "websocket.log"));
        }
        FileHandler fh = new FileHandler(basePath + "websocket.log",true);
        logger.addHandler(fh);
        logger.setUseParentHandlers(false);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);

        //On ajoute la référence à l'icône dans la barre de notification pour pouvoir afficher les messages
        this.trayIcon = trayIcon;
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        conn.send("connected"); //This method sends a message to the new client
        logger.info("new connection to " + conn.getRemoteSocketAddress());
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        logger.info("closed " + conn.getRemoteSocketAddress() + " with exit code " + code + " additional info: " + reason);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        //Si le message reçu annonce une signature, on prépare les variables utiles
        prepareToSign = message.startsWith("sign://");
        checksum = message.replace("sign://", "");
        logger.info("received message from " + conn.getRemoteSocketAddress() + ": " + message);
    }

    @Override
    public void onMessage(WebSocket conn, ByteBuffer message) {
        //On ne reçoit un ByteBuffer que dans le cas d'une réception d'un fichier à signer, si le fichier n'a pas été annoncé, on ignore
        logger.info("received ByteBuffer from " + conn.getRemoteSocketAddress());
        if (prepareToSign) {
            trayIcon.displayMessage("Signature CPS TWM", "Réception d'un fichier à signer...", TrayIcon.MessageType.INFO);
            logger.info("sign buffer");
            try {
                String fileName = basePath + "download";
                String signatureName = basePath + "download.sig";
                Files.deleteIfExists(Paths.get(fileName));
                Files.deleteIfExists(Paths.get(signatureName));

                File dest = new File(fileName);
                dest.createNewFile();

                //On commence par récupérer le fichier
                try (ByteArrayInputStream sourceFile = new ByteArrayInputStream(message.array())) {
                    try (FileOutputStream destinationFile = new FileOutputStream(dest)) {
                        byte[] buffer = new byte[message.capacity()];
                        int nbLecture;

                        while ((nbLecture = sourceFile.read(buffer)) != -1) {
                            destinationFile.write(buffer, 0, nbLecture);
                        }
                    }

                    if (dest.exists()) {
                        //On vérifie le checksum du fichier téléchargé
                        if (checksum.equals(getChecksum(fileName))) {
                            logger.info("checksum ok");
                            conn.send("checksum ok");
                            //On envoi la signature au client
                            GenSig.genSig(fileName, signatureName);
                            if (Files.exists(Paths.get(signatureName))) {
                                conn.send("sig://" + getSignatureHexa(signatureName));
                                logger.info("signature send");
                                trayIcon.displayMessage("Signature CPS TWM", "Signature réussie", TrayIcon.MessageType.INFO);
                            } else {
                                logger.severe("signature échouée");
                                conn.send("sig:////ERROR//");
                                trayIcon.displayMessage("Signature CPS TWM", "Signature échouée", TrayIcon.MessageType.WARNING);
                            }
                        } else {
                            //Si le checksum du fichier n'est pas vérifié, on renvoi une erreur au client
                            logger.severe("checksum ko : " + getChecksum(fileName));
                            conn.send("checksum ko");
                            conn.send("sig:////ERROR//");
                            trayIcon.displayMessage("Signature CPS TWM", "Signature échouée", TrayIcon.MessageType.WARNING);
                        }
                    } else {
                        //Si le fichier n'a pas été téléchargé, on renvoi une erreur au client
                        conn.send("sig:////ERROR//");
                        trayIcon.displayMessage("Signature CPS TWM", "Signature échouée", TrayIcon.MessageType.WARNING);
                    }
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                    conn.send("sig:////ERROR//");
                }
            } catch (IOException e) {
                e.printStackTrace();
                conn.send("sig:////ERROR//");
            }
            prepareToSign = false;

            //On ferme la connexion une fois la réponse de la signature envoyée
            conn.close();
        }
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        if (conn == null) {
            logger.severe("an error occured on connection " + ex.getMessage());
        } else {
            logger.severe("an error occured on connection " + conn.getRemoteSocketAddress() + ":" + ex.getMessage());
        }
    }

    @Override
    public void onStart() {
        logger.info("server started successfully : " + this.getAddress().toString());
    }

    private String getChecksum(String fileName) throws NoSuchAlgorithmException, IOException {
        //Calcule le checksum d'un fichier dont le chemin est passé en paramètre
        File file = new File(fileName);
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        InputStream fis = new FileInputStream(file);
        int n = 0;
        byte[] buffer = new byte[8192];
        while (n != -1) {
            n = fis.read(buffer);
            if (n > 0) {
                digest.update(buffer, 0, n);
            }
        }
        fis.close();
        return byteArrayToHexa(digest.digest());
    }

    private String getSignatureHexa(String signatureName) throws IOException {
        //On transforme le fichier en hexa pour le renvoyer au client
        String signature;

        byte[] binary = Files.readAllBytes(Paths.get(signatureName));
        signature = byteArrayToHexa(binary);
        return signature;
    }

    private String byteArrayToHexa(byte[] bytes) {
        //Transforme un byteArray (ByteBuffer) en hexa
        String hexa;
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02X", b));
        }
        hexa = sb.toString();
        hexa = hexa.toLowerCase();

        return hexa;
    }

    protected static SSLContext getSslContext() throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, KeyManagementException {
        //On prépare les variables
        String STORETYPE = "PKCS12";
        String KEYSTORE = "/WebSocketCert.p12";
        String STOREPASSWORD = "maltainfo";
        String KEYPASSWORD = "maltainfo";

        //On initialise le Keystore avec le certificat présent dans les ressources
        KeyStore ks = KeyStore.getInstance(STORETYPE);
        InputStream resourceStream = SimpleServer.class.getResourceAsStream(KEYSTORE);
        ks.load(resourceStream, STOREPASSWORD.toCharArray());

        //On initialise les variables nécessaire pour récupérer le contexte
        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(ks, KEYPASSWORD.toCharArray());
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        tmf.init(ks);

        //On créé le contexte SSL
        SSLContext sslContext;
        sslContext = SSLContext.getInstance("TLS");
        sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

        return sslContext;
    }
}
