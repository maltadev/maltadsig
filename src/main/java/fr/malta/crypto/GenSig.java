package fr.malta.crypto;

import fr.malta.crypto.keystore.KeyStoreUtil;
import sun.security.pkcs.ContentInfo;
import sun.security.pkcs.PKCS7;
import sun.security.pkcs.SignerInfo;
import sun.security.util.DerOutputStream;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;

import javax.swing.*;
import java.awt.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class GenSig {
    private static String basePath;

    public static void genSig(String filePath, String signPath) throws IOException {
        if (!System.getProperty("os.name").equals("Mac OS X")) {
            basePath = "";
        } else {
            basePath = "/tmp/";
        }

        //On prépare le fichier de log
        Logger logger = Logger.getLogger("LogSignature");
        FileHandler fh;
        if (!Files.exists(Paths.get(basePath+"signature.log"))) {
            Files.createFile(Paths.get(basePath+"signature.log"));
        }
        fh = new FileHandler(basePath+"signature.log",true);
        logger.addHandler(fh);
        logger.setUseParentHandlers(false);
        SimpleFormatter formatter = new SimpleFormatter();
        fh.setFormatter(formatter);

        try {
            logger.info("=== Signature du fichier '" + filePath + "'");
            logger.info(System.getProperty("os.name"));

            String codePorteur = getCodePorteur(logger);

            KeyStore ks = KeyStoreUtil.getKeyStore(codePorteur);

            //On remplit le tableau avec les alias des certificats pour demande à l'utilisateur de choisir
            ArrayList<String> certificatesAlias = new ArrayList<>();
            Enumeration en = ks.aliases();
            while (en.hasMoreElements()) {
                String aliasKey = (String) en.nextElement();
                X509Certificate c = (X509Certificate) ks.getCertificate(aliasKey);

                if (c.getExtendedKeyUsage() != null) {
                    for (String s : c.getExtendedKeyUsage()) {
                        //OID pour la signature de message électronique (contenu dans la carte CPS)
                        if (s.equals("1.3.6.1.5.5.7.3.4")) {
                            logger.info(aliasKey + " : " + s + " (ok)");
                            certificatesAlias.add(aliasKey);
                        } else {
                            logger.severe(aliasKey + " : " + s + " (ko)");
                        }
                    }
                } else {
                    logger.severe(aliasKey + " : Aucun KeyUsage (ko)");
                }
            }

            if (!certificatesAlias.isEmpty()) {
                String choice;
                if (certificatesAlias.size() > 1) {
                    //On créé la boîte de dialogue pour choisir le certificat
                    JOptionPane p = new JOptionPane("Choisir le certificat de signature : ", JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION, null);
                    //On paramètre la boîte de dialogue pour afficher une liste déroulante
                    p.setSelectionValues(certificatesAlias.toArray());
                    p.setWantsInput(true);
                    JDialog d = p.createDialog("Choisir un certificat");
                    //On met la boîte de dialogue devant toutes les autres fenêtre du système
                    d.setAlwaysOnTop(true);
                    d.setVisible(true);
                    d.dispose();
                    //L'utilisateur choisit le certificat qu'il veut utiliser pour signer le fichier
                    choice = (String) p.getInputValue();
                } else {
                    //S'il n'y a qu'un certificat dans la liste, on le choisit par défaut
                    choice = certificatesAlias.get(0);
                }

                X509Certificate crt = (X509Certificate) ks.getCertificate(choice);

                if (crt != null) {
                    logger.info("Certificat choisi : " + choice);

                    //On récupère la clé privée
                    PrivateKey privateKey = (PrivateKey) ks.getKey(choice, codePorteur.toCharArray());

                    //on lit le fichier que l'on veut signer
                    byte[] content = Files.readAllBytes(Paths.get(filePath));

                    //On créé l'objet signature
                    Signature sig = Signature.getInstance(crt.getSigAlgName());
                    sig.initSign(privateKey);
                    sig.update(content);
                    byte[] signedData = sig.sign();

                    //load X500Name
                    X500Name xName = X500Name.asX500Name(crt.getIssuerX500Principal());
                    //load serial number
                    BigInteger serial = crt.getSerialNumber();
                    //laod digest algorithm
                    AlgorithmId digestAlgorithmId = AlgorithmId.get(AlgorithmId.getDigAlgFromSigAlg(crt.getSigAlgName()));
                    //load signing algorithm
                    AlgorithmId signAlgorithmId = AlgorithmId.get(AlgorithmId.getEncAlgFromSigAlg(crt.getSigAlgName()));
                    //Create SignerInfo:
                    SignerInfo sInfo = new SignerInfo(xName, serial, digestAlgorithmId, signAlgorithmId, signedData);

                    //Create empty ContentInfo:
                    ContentInfo cInfo = new ContentInfo(ContentInfo.DATA_OID, null);
                    //Create PKCS7 Signed data
                    PKCS7 p7 = new PKCS7(new AlgorithmId[]{digestAlgorithmId}, cInfo,
                            new java.security.cert.X509Certificate[]{crt},
                            new SignerInfo[]{sInfo});
                    //Write PKCS7 to ByteArray
                    ByteArrayOutputStream bOut = new DerOutputStream();
                    p7.encodeSignedData(bOut);
                    Files.write(Paths.get(signPath), bOut.toByteArray());

                    logger.info("=== Signature réussie ('" + signPath + "')");
                } else {
                    logger.severe("=== Signature échouée : aucun certificat choisi");
                }
            } else {
                logger.severe("=== Signature échouée : aucun certificat valide");
            }
        } catch (Exception e) {
            logger.severe("=== Signature échouée : " + e + " : "+ Arrays.toString(e.getStackTrace()));
        }

        fh.close();
    }

    private static String getCodePorteur(Logger logger) {
        String codePorteur = "";
        //On créé la fenêtre de dialogue manuellement
        JOptionPane jOptionPane = new JOptionPane();
        JPanel passwordPanel = new JPanel(new GridLayout(2, 1));
        JLabel label = new JLabel("Saisir le code porteur de la carte CPS");
        JPasswordField pwd = new JPasswordField(12);
        passwordPanel.add(label);
        passwordPanel.add(pwd);
        //Paramètres de la boîte de dialogue
        jOptionPane.setOptionType(JOptionPane.OK_CANCEL_OPTION);
        jOptionPane.setMessageType(JOptionPane.PLAIN_MESSAGE);
        jOptionPane.setMessage(passwordPanel);
        JDialog jDialog = jOptionPane.createDialog("Code porteur CPS");
        //On met la boîte de dialogue devant toutes les autres fenêtre du système
        jDialog.setAlwaysOnTop(true);
        jDialog.setVisible(true);
        jDialog.dispose();

        //On récupère les informations saisies
        int action = (int) jOptionPane.getValue();
        if (action < 0) {
            logger.info("Code porteur non saisi");
        } else {
            codePorteur = new String(pwd.getPassword());
        }

        return codePorteur;
    }
}
