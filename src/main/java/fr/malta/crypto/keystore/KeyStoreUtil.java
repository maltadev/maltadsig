package fr.malta.crypto.keystore;

import sun.security.pkcs11.wrapper.PKCS11;
import sun.security.pkcs11.wrapper.PKCS11Exception;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;

public class KeyStoreUtil {
    public static KeyStore getKeyStore(String codePorteur) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, PKCS11Exception {
        //Le code est quasiment le même pour Windows et pour Mac, la seule différence étant le chemin vers la cryptolib
        String osName = System.getProperty("os.name");
        String javaArch = System.getProperty("sun.arch.data.model");
        KeyStore ks = null;
        String libName;
        if (osName.toLowerCase().contains("windows")) {
            //if (javaArch.equals("64")) {
            if (System.getenv("ProgramFiles(x86") != null) {
                libName = "C:\\Windows\\System32\\cps3_pkcs11_w64.dll";
            } else {
                libName = "C:\\Windows\\System32\\cps3_pkcs11_w32.dll";
            }
        } else {
            libName = "/usr/local/lib/libcps3_pkcs11_osx.dylib";
        }

        if (codePorteur != null && !codePorteur.equals("")) {
            //On récupère le slot de la carte
            long slot = 0;
            PKCS11 p11;

            //Pire astuce pour appeler le code deux fois... (si le premier crash, le second normalement fonctionne...)
            try {
                p11 = PKCS11.getInstance(libName, "C_GetFunctionList", null, false);
                long[] slots = p11.C_GetSlotList(true);
                if (slots.length > 0) {
                    slot = slots[0];
                }
            } catch (Exception e) {
                p11 = PKCS11.getInstance(libName, "C_GetFunctionList", null, false);
                long[] slots = p11.C_GetSlotList(true);
                if (slots.length > 0) {
                    slot = slots[0];
                }
            }

            //On récupère le keystore de la carte CPS grâce à la cryptolib
            String cardConfig = "name = CPS\n" +
                    "library = "+libName+"\n"+
                    "slotListIndex = "+slot;
            InputStream is = new ByteArrayInputStream(cardConfig.getBytes());
            Provider securityProvider = new sun.security.pkcs11.SunPKCS11(is);
            Security.removeProvider(securityProvider.getName());
            Security.addProvider(securityProvider);

            ks = KeyStore.getInstance("PKCS11");
            ks.load(null, codePorteur.toCharArray());
        }

        return ks;
    }
}
