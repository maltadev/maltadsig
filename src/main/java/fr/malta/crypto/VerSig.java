package fr.malta.crypto;

import sun.security.pkcs.PKCS7;
import sun.security.pkcs.SignerInfo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class VerSig {
    public static void checkSignature() throws IOException {
        PKCS7 p7 = new PKCS7(Files.readAllBytes(Paths.get("C:\\TEST_DATA\\signature.sig")));
        PKCS7 p7wd = new PKCS7(Files.readAllBytes(Paths.get("C:\\TEST_DATA\\signature.windev.sig")));

        System.out.println("===JAVA");
        for (SignerInfo signerInfo : p7.getSignerInfos()) {
            System.out.println(signerInfo.getIssuerName());
        }

        System.out.println("===WD");
        for (SignerInfo signerInfo : p7wd.getSignerInfos()) {
            System.out.println(signerInfo.getIssuerName());
        }
    }
}
